Rails.application.routes.draw do
  root to: 'links#index'    
  devise_for :users
  resources :links, only: [:new, :index, :create]

  get '/:slug' => 'links#show'
  get '/links/detail/:slug' => 'links#detail'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  
end
