module UseCase
  class CreateLink
    attr_reader :given_url, :user_id

    def initialize(args)
      @given_url = args["given_url"]
      @user_id = args["user_id"]
    end

    def call
      link = nil
      while link.nil?
        link = create_link
      end
      link
    end

    private

    def create_link
      begin
        link = Link.create(slug: generate_slug, given_url: given_url, user_id: user_id)
        write_to_cache(link)
      rescue
        link = nil
      end
      link
    end

    def write_to_cache link
      Rails.logger.info "CacheStore::Key=#{link.slug} started"
      response = Rails.cache.write(link.slug, link, expires_in: 5.hours)
      Rails.logger.info "Cache::Key=#{link.slug} finished status=#{response}"
    end

    def generate_slug
      SecureRandom.hex(10)
    end
  end
end