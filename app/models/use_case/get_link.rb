module UseCase
  class GetLink
    attr_reader :slug
    
    def initialize(slug:)
      @slug = slug
    end

    def call
      link = find_in_cache
      link = find_in_db if link.nil?
      update_count link unless link.nil?
      link
    end

    private 

    def find_in_cache
      Rails.logger.info "CacheFetch::Key=#{slug} started"
      link = Rails.cache.read(slug)
      Rails.logger.info "CacheFetch::Key=#{slug} finished response = #{link}"
      link
    end
    
    def find_in_db
      link = Link.find_by(slug: slug)
      store_in_cache link unless link.nil?
      link
    end

    def store_in_cache link
      Rails.logger.info "CacheStore::Key=#{link.slug} started"
      response = Rails.cache.write(link.slug, link, expires_in: 5.hours)
      Rails.logger.info "Cache::Key=#{link.slug} finished status=#{response}"
    end

    def update_count link
      link.increment!(:use_count)
    end
    
  end
end